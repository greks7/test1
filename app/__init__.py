import os
from flask import Flask
# from flask.ext.admin import Admin
# from flask.ext.admin.contrib.sqlamodel import ModelView

from .database import db
from flask_bootstrap import Bootstrap

def create_app():
    app = Flask(__name__)
    bootstrap = Bootstrap(app)
    # app.config.from_object(os.environ['APP_SETTINGS'])
    # admin=Admin(app)
    app.config.from_object('config.DevelopmentConfig')
    
    db.init_app(app)
    with app.test_request_context():
        from app.post.models import Post ,Comment,VideoViews
        db.create_all()
    
   
        # if app.debug == True:
        # try:
        #     from flask_debugtoolbar import DebugToolbarExtension
        #     toolbar = DebugToolbarExtension(app)
        # except:
        #     pass

    #import app.video.controllers as video
    from app.post.views import posts
    import app.main.views as main
    from app.auth.admin import admin
    from app.video.views import videos
    # import app.admin.controllers as admin
    app.register_blueprint(main.module)
    app.register_blueprint(videos)
    #app.register_blueprint(video.module)
    app.register_blueprint(posts)
    app.register_blueprint(admin)
    #app.register_blueprint(api)

    return app
