from flask import Blueprint, request, redirect, render_template, url_for
from flask.views import MethodView

from wtforms.ext.sqlalchemy.orm import model_form

from app.auth.auth import requires_auth
from app.post.models import Post, Comment ,VideoViews
from app.database import db
from app.auth.form import CKEditorForm

admin = Blueprint('admin', __name__, template_folder='templates')
# class Video(MethodView):
#     decorators = [requires_auth]
#     cls = VideoViews
#     def get(self):
        
#         videos = self.cls.query.all()
#         return render_template('admin/videos/list.html',videos=videos)
# class VideoDetail(MethodView):
#     decorators = [requires_auth]
#     def get_context(self,slug):
#         form_cls = model_form(VideoViews, db.session)

#         if slug:
#             video = VideoViews.query.filter_by(slug=slug).first()
#             #post = Post.query.get_or_404(slug=slug)
#             if request.method == 'POST':
#                 form = form_cls(request.form)# inital=post.data)
#             else:
#                 form = form_cls(obj=video)
#         else:
#             video = VideoViews()
#             form = form_cls(request.form)

#         context = {
#             "video": video,
#             "form": form,
#             "create": slug is None
#         }
#         return context

#     # def get(self, slug):
#     #     context = self.get_context(slug)
#     #     return render_template('admin/videos/detail.html', **context)

#     def video(self, slug):
#         context = self.get_context(slug)
#         form = context.get('form')

#         if form.validate():
#             video = context.get('video')
#             form.populate_obj(video)
#             db.session.add(video)
#             #post.save()
#             db.session.commit()
#             return redirect(url_for('admin.index'))
#         return render_template('admin/videos/detail.html', **context)




class List(MethodView):
    decorators = [requires_auth]
    cls = Post

    def get(self):
        posts = self.cls.query.all()
        return render_template('admin/list.html', posts=posts)

class Detail(MethodView):

    decorators = [requires_auth]
    # Map post types to models
    class_map = {
        'post': Post,
        
        
    }

    # def get_context(self, slug=None):

    #     if slug:
    #         post = Post.query.filter_by(slug=slug)
    #         # Handle old posts types as well
    #         cls = post.__class__ if post.__class__ != Post else VideoViews
    #         form_cls = model_form(cls,db.session,exclude=('created_at','other','comment'))
    #         if request.method == 'POST':
    #             form = form_cls(request.form) #inital=post._data)
    #         else:
    #             form = form_cls(obj=post)
    #     else:
    #         # Determine which post type we need
    #         cls = self.class_map.get(request.args.get('type', 'post','video'))
    #         post = cls()
    #         form_cls = model_form(cls,db.session,exclude=('created_at','other', 'comments'))
    #         form = form_cls(request.form)
    #     context = {
    #         "post": post,
    #         "form": form,
    #         "create": slug is None
    #     }
    #     return context
    def get_context(self, slug=None):
        form_cls = model_form(Post, exclude=('created_at', 'comments'))

        if slug:
            post = Post.query.filter_by(slug=slug)
            if request.method == 'POST':
                form = form_cls(request.form)# inital=post._data)
            else:
                form = form_cls(obj=post)
        else:
            post = Post()
            form = form_cls(request.form)

        context = {
            "post": post,
            "form": form,
            "create": slug is None
        }
        return context




    def get(self, slug):
        context = self.get_context(slug)
        return render_template('admin/detail.html', **context)

    def post(self, slug):
        context = self.get_context(slug)
        form = context.get('form')

        if form.validate():
            post = context.get('post')
            form.populate_obj(post)
            db.session.add(post)
            #post.save()
            db.session.commit()
            return redirect(url_for('admin.index'))
        return render_template('admin/detail.html', **context)





# Register the urls
admin.add_url_rule('/admin/', view_func=List.as_view('index'))
admin.add_url_rule('/admin/create/', defaults={'slug': None}, view_func=Detail.as_view('create'))
admin.add_url_rule('/admin/<slug>/', view_func=Detail.as_view('edit'))
# admin.add_url_rule('/admin/video/create/',defaults={'slug': None},view_func=List.as_view('video.create'))
# admin.add_url_rule('/admin/video/<slug>/',view_func=Detail.as_view('video.edit'))
