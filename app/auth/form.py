

from flask_wtf import Form
from wtforms import TextAreaField, SubmitField, StringField
from app.auth.flaskckeditor import CKEditor

class CKEditorForm(Form, CKEditor):
    title =  StringField()
    ckdemo = TextAreaField()
    submit = SubmitField('submit')