from flask import Blueprint, request, redirect, render_template, url_for
from flask.views import MethodView
from app.post.models import Post, Comment,VideoViews
from app.database import db 
from wtforms.ext.sqlalchemy.orm import model_form
from jinja2 import Undefined, Template
from flask_paginate import Pagination, get_page_parameter
import datetime

# 
#from wtforms.ext.sqlalchemy.orm import model_form
posts = Blueprint('posts', __name__, template_folder='templates')


class ListView(MethodView):
    
    # 
    
    # def get(self,page=1):
    #     per_page = 2
    #     posts = Post.query.order_by(Post.created_at.desc()).paginate(page,per_page,error_out=False)
    #     # return render_template('posts/list.html',posts=posts)
    #     #posts = db.session.query(Post).paginate(page, POSTS_PER_PAGE, False)
    #     #posts = Post.query.order_by(Post).paginate(page,per_page,error_out=False)
    #     return render_template('posts/list.html',posts=posts)
    def get(self,page=1):
        # posts = Post.query.order_by('-create_at')
        per_page = 2
        posts = Post.query.order_by(Post.created_at.desc()).paginate(page,per_page,error_out=False)
    
        

    #comments = Comment.query.all()
        return render_template('posts/list.html',posts=posts)


# class DetailView(MethodView):

#     def get(self,slug):
#         post = Post.query.filter_by(slug=slug).first()
#         #comen = Post.query.get(slug=slug)
#         if post:
#             return render_template("posts/detail.html", post=post)

#             abort(404)
#         return render_template('posts/detail.html', post=post)

class DetailView(MethodView):
    #methods = ['GET', 'POST']
    
        
    form = model_form(Comment,db.session,exclude=['created_at','owner'])

    def get_context(self, slug):
        #post = Post.objects.get_or_404(slug=slug)
        post = Post.query.filter_by(slug=slug).first()

        form = self.form(request.form)

        context = {
            "post": post,
            "form": form,
            
        }
        return context

    def get(self, slug):
        
        context = self.get_context(slug)
        return render_template('posts/detail.html', **context)

    def post(self, slug):
        #if  request.methods == 'POST':
        context = self.get_context(slug)
        form = context.get('form')
        

        if request.method == 'POST' and form.validate():
            comment = Comment()
        
            # form =newform (form.body.data, form.author.data,
            #         form.created_at.data)
        
            #comment = Comment()
            form.populate_obj(comment)

            post = context.get('post')
            post.comments.append(comment)
            #post.save()
            db.session.add(comment)
            db.session.commit()
            return redirect(url_for('posts.detail', **context,slug=slug))

        return render_template('posts/detail.html')
    

# Register the urls
posts.add_url_rule('/', view_func=ListView.as_view('lists'))
posts.add_url_rule('/<int:page>/', view_func=ListView.as_view('list'))

posts.add_url_rule('/<slug>/', view_func=DetailView.as_view('detail'))
