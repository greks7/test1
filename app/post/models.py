import datetime
from flask import url_for
from sqlalchemy import ForeignKey
from sqlalchemy.orm import backref
from sqlalchemy import Integer, String, Text, Float, Date
from  app.database import db
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import create_engine


class Post(db.Model):
    __tablename__ = 'post'

    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String())
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    
    slug = db.Column(db.String(255))
    title = db.Column(db.String(255))
    comments = db.relationship('Comment',backref=db.backref('owner'))
    image_url = db.Column(db.String())
    embed_code = db.Column(db.String())
    #video = db.relationship('VideoViews',backref=db.backref('other'))
    
    #blogpost = db.relationship('BlogPost',backref=db.backref('owner_body'))



    #     self.comments = comments
    class Meta:
        order_by = ('-created_at',)

# class BlogPost(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     body = db.Column(db.String())
#     owner_body_id = db.Column(db.Integer,db.ForeignKey('post.id'))

  

    # class BlogPost(Post):
    #     body = db.StringField(verbose_name=u"Текст", required=True)
    #
    # class Video(Post):
    #     embed_code = db.StringField(verbose_name=u"Код для блога", required=True)
    # #
    # class Image(Post):
    #     image_url = db.StringField(verbose_name=u"URL картинки", required=True, max_length=255)

    # class Quote(Post):
    #     body = db.StringField(verbose_name=u"Цитата", required=True)
    #     author = db.StringField(verbose_name=u"Имя автора", required=True, max_length=255)
# class Video(Post):
#     embed_code = db.Column(db.String(255))


# class Image(Post):
#     image_url = db.Column(db.String())
class Comment(db.Model):
    __tablename__ = 'comment'

    id = db.Column(db.Integer, primary_key=True)
    
    body = db.Column(db.String())
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    
    title = db.Column(db.String())
    author = db.Column(db.String())
    owner_id = db.Column(db.Integer, db.ForeignKey('post.id'))
   



    def __repr__(self):
        return '<Post %r>' % (self.body)


class VideoViews(db.Model):
    __tablename__ = 'video'

    id = db.Column(db.Integer,primary_key=True)
    video_link = db.Column(db.String())
    title = db.Column(db.String())
    slug = db.Column(db.String())
    image_tumb = db.Column(db.String())
    dyration = db.Column(db.String())
    #other_id = db.Column(db.Integer, db.ForeignKey('post.id'))








