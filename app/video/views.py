from flask import Blueprint, request, redirect, render_template, url_for
from flask.views import MethodView
from app.post.models import VideoViews
from app.database import db 
from wtforms.ext.sqlalchemy.orm import model_form
from jinja2 import Undefined, Template
from flask_paginate import Pagination, get_page_parameter
import datetime

from flask import g



videos = Blueprint('videos', __name__, template_folder='templates')
class VideoList(MethodView):
    def get(self):
        tvs = VideoViews.query.all()
        return render_template("video/tvlist.html",tvs=tvs) 

# class VideoView(MethodView):
    
#     # 
    
#     # def get(self,page=1):
#     #     per_page = 2
#     #     posts = Post.query.order_by(Post.created_at.desc()).paginate(page,per_page,error_out=False)
#     #     # return render_template('posts/list.html',posts=posts)
#     #     #posts = db.session.query(Post).paginate(page, POSTS_PER_PAGE, False)
#     #     #posts = Post.query.order_by(Post).paginate(page,per_page,error_out=False)
#     #     return render_template('posts/list.html',posts=posts)
#     def get(self,page=1):

#         videos = VideoViews.query.paginate(page=page,per_page=3)
#         return render_template('video/videolist.html',videos=videos)
class DetailVideo(MethodView):

    def get(self,slug):
        #video = VideoViews.query.order_by().first()
        video = VideoViews.query.filter_by(slug=slug).first()
         #post = Post.query.filter_by(slug=slug).first()
        
        return render_template("video/tvditail.html", video=video)




#videos.add_url_rule('/videos/', view_func=VideoView.as_view('videolists'))
#videos.add_url_rule('/videos/<int:page>/',view_func=VideoView.as_view('videolist'))
videos.add_url_rule('/tv/<slug>',view_func=DetailVideo.as_view('tvditail'))
videos.add_url_rule('/tv/',view_func=VideoList.as_view('tvlist'))