import os
# pagination
from sqlalchemy import create_engine
POSTS_PER_PAGE = 2
class Config(object):
    # Определяет, включен ли режим отладки
    # В случае если включен, flask будет показывать
    # подробную отладочную информацию. Если выключен -
    # - 500 ошибку без какой либо дополнительной информации.
    DEBUG = False
    # Включение защиты против "Cross-site Request Forgery (CSRF)"
    CSRF_ENABLED = True
    # Случайный ключ, которые будет исползоваться для подписи
    # данных, например cookies.
    SECRET_KEY = 'YOUR_RANDOM_SECRET_KEY'
    # URI используемая для подключения к базе данных
    SQLALCHEMY_DATABASE_URI='postgresql://lukorjuvqwssoj:de00c9d75f755a73510e2df3807d52153705b1a207cd69ba56132c96fbd0def2@ec2-23-23-248-162.compute-1.amazonaws.com:5432/d9suo5qbkp978g'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    

class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
